package com.druva.syncdata.Retrofit;

import com.druva.syncdata.Model.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitApi {

    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: */*"})
   // @FormUrlEncoded
    @POST("add_users.php")
    Call<JSONObject> updatenewsyncdata(@Body String items);
}
