package com.druva.syncdata.Model;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    private String name;
    private String email;
    private String phone;
    private String date;

    public User(String name, String email, String phone,String date) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

   /* @Override
    public String toString() {
        return '{' +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", email:'" + email + '\'' +
                ", phone:'" + phone + '\'' +
                ", date:'" + date + '\'' +
                '}';
    }*/


    @Override
    public String toString() {
        return '{' +
                " id: " + id +
                ", name: '" + name + '\'' +
                ", email: '" + email + '\'' +
                ", phone: '" + phone + '\'' +
                ", date: '" + date + '\'' +
                '}';
    }
}
