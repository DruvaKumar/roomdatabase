package com.druva.syncdata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.druva.syncdata.Data.UserDao;
import com.druva.syncdata.Data.UserDataBase;
import com.druva.syncdata.Model.User;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {
    TextInputEditText editTextname, editTextPhonenumber ,editTextEmail;
    Button buttonRegister;
    TextView textViewLogin;
    private UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        editTextname = findViewById(R.id.editTextname);
        editTextPhonenumber = findViewById(R.id.editTextPhone);
        editTextEmail = findViewById(R.id.editTextEmail);

        buttonRegister = findViewById(R.id.buttonRegister);

        textViewLogin = findViewById(R.id.textViewLogin);
        textViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        userDao = Room.databaseBuilder(this, UserDataBase .class, "datasync.db").allowMainThreadQueries()
                .build().getUserDao();

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextname.getText().toString().trim();
                String phone = editTextPhonenumber.getText().toString().trim();
                String email = editTextEmail.getText().toString().trim();
                String date= new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

                if(name.isEmpty()){
                    editTextname.setError("Please enter Name ");
                }
                else if(phone.isEmpty()|| phone.length()<10)
                {
                    editTextPhonenumber.setError("Please enter valid Mobile Number ");
                }
                else if (!email.contains("@"))
                {
                    editTextEmail.setError("Please enter valid Email ID ");
                }
                else{

                    User user = userDao.checkUser(phone);
                    if (user == null) {
                       user = new User(name,email,phone,date);
                        userDao.insert(user);

                        editTextname.setText("");
                        editTextPhonenumber.setText("");
                        editTextEmail.setText("");

                        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                        intent.putExtra("User", user);
                        startActivity(intent);
                    }else{
                        Toast.makeText(RegisterActivity.this, "Mobile number already registered", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });
    }
}
