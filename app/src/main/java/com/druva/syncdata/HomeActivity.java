package com.druva.syncdata;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.room.Room;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.TextView;

import com.druva.syncdata.Data.UserDao;
import com.druva.syncdata.Data.UserDataBase;
import com.druva.syncdata.Helpers.AlarmReceiver;
import com.druva.syncdata.Helpers.MyService;
import com.druva.syncdata.Helpers.Session;
import com.druva.syncdata.Model.User;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomeActivity extends AppCompatActivity {
    private TextView tvUser;
    Session session;

    private User user;
    UserDao db;
    UserDataBase dataBase;

    MediaPlayer mp=null ;
    private PendingIntent pendingIntent;
    AlarmManager alarmManager;
    Intent alarmIntent;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_home);
        session = new Session(HomeActivity.this);

        user = (User) getIntent().getSerializableExtra("User");

        tvUser = findViewById(R.id.tvUser);

        dataBase = Room.databaseBuilder(this, UserDataBase.class, "datasync.db")
                .allowMainThreadQueries()
                .build();


        db = dataBase.getUserDao();

        if (user != null) {
            tvUser.setText("WELCOME\n " + user.getName());
        }

        if(!session.isalarm())
        {
            stratalarm();

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void stratalarm() {
        session.setalarm(true);
        alarmIntent = new Intent(HomeActivity.this, AlarmReceiver.class);
        alarmIntent.setAction("alarm.running");
        pendingIntent = PendingIntent.getBroadcast(HomeActivity.this, 0, alarmIntent, 0);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        long interval = 5 * 60 * 1000;

        Calendar calendar = Calendar.getInstance();
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis()+interval,interval, pendingIntent);
    }

}


