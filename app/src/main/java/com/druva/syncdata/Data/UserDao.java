package com.druva.syncdata.Data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.druva.syncdata.Model.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM User where email= :email and phone= :phone")
    User getUser(String email, String phone);

    @Query("SELECT * FROM User where phone= :phone")
    User checkUser(String phone);

    @Query("SELECT * FROM User")
    List<User> getAll();

    @Query("SELECT * FROM User where date  BETWEEN :start AND :end ")
    List<User> getsycdata(String start , String end);

    @Insert
    void insert(User user);

    @Update
    void update(User user);

    @Delete
    void delete(User user);
}
