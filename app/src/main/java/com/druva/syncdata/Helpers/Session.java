package com.druva.syncdata.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences prefe;
    SharedPreferences.Editor editor;
    Context ctx;

    public Session(Context ctx){
        this.ctx=ctx;
        prefe=ctx.getSharedPreferences("MYDB", Context.MODE_PRIVATE);
        editor= prefe.edit();
    }

    public void setalarm(boolean alarm){
        editor.putBoolean("alarmInmode",alarm);
        editor.commit();
    }

    public boolean isalarm()
    {

        return prefe.getBoolean("alarmInmode",false);
    }


}
