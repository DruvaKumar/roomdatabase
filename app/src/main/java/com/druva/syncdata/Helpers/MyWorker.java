package com.druva.syncdata.Helpers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.room.Room;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.druva.syncdata.Data.UserDao;
import com.druva.syncdata.Data.UserDataBase;
import com.druva.syncdata.LoginActivity;
import com.druva.syncdata.Model.User;
import com.druva.syncdata.R;
import com.druva.syncdata.Retrofit.RetrofitClient;
import com.google.gson.Gson;


import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyWorker extends Worker {

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }
    @NonNull
    @Override
    public Result doWork() {

        UserDao db;
        UserDataBase dataBase;
        List<User> syncuserss=new ArrayList<>();

        dataBase = Room.databaseBuilder(getApplicationContext(), UserDataBase.class, "datasync.db")
                .allowMainThreadQueries()
                .build();
        db = dataBase.getUserDao();

        String start= new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        String end= new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(-5)));

        syncuserss=db.getsycdata(end, start);
      //  syncuserss=db.getAll();
       // Log.v("TAG"," sync users are "+new Gson().toJson(syncuserss));



        Call<JSONObject> call = RetrofitClient.getInstance().getMyApi().updatenewsyncdata(new Gson().toJson(syncuserss));
        final List<User> finalSyncuserss = syncuserss;
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(@NotNull Call<JSONObject> call, @NotNull Response<JSONObject> response) {
                Log.v("TAG","Response is success"+response.toString());
                // Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
                String NOTIFICATION_CHANNEL_ID = "10001" ;

                Intent notificationIntent = new Intent(getApplicationContext(), LoginActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle("Syncing new Users Data ")
                                .setContentText("Successfully uploded New users data to server ");


                NotificationManager notificationManager =
                        (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

                // Since android Oreo notification channel is needed.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                            "Vidwath App",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }
                Random r = new Random();
                int randomNumber = r.nextInt(10);
                notificationManager.notify(randomNumber , notificationBuilder.build());

            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.v("TAG","Response is failure "+t);

                String NOTIFICATION_CHANNEL_ID = "10001" ;
                Intent notificationIntent = new Intent(getApplicationContext(), LoginActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle("Syncing new Users Data ")
                                .setContentText("Failed to upload data to server");


                NotificationManager notificationManager =
                        (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

                // Since android Oreo notification channel is needed.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                            "Vidwath App",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }
                Random r = new Random();
                int randomNumber = r.nextInt(10);
                notificationManager.notify(randomNumber , notificationBuilder.build());

            }
        });



        return null;
    }
}
